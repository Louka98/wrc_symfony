<?php

namespace App\Repository;

use App\Entity\Speciale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Speciale|null find($id, $lockMode = null, $lockVersion = null)
 * @method Speciale|null findOneBy(array $criteria, array $orderBy = null)
 * @method Speciale[]    findAll()
 * @method Speciale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecialeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Speciale::class);
    }

    // /**
    //  * @return Speciale[] Returns an array of Speciale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Speciale
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

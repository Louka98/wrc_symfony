<?php

namespace App\Repository;

use App\Entity\Rallye;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Rallye|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rallye|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rallye[]    findAll()
 * @method Rallye[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RallyeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rallye::class);
    }

    // /**
    //  * @return Rallye[] Returns an array of Rallye objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rallye
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

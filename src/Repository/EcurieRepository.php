<?php

namespace App\Repository;

use App\Entity\Ecurie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ecurie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ecurie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ecurie[]    findAll()
 * @method Ecurie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcurieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ecurie::class);
    }

    // /**
    //  * @return Ecurie[] Returns an array of Ecurie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ecurie
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Controller;

use App\Entity\Ecurie;
use App\Entity\Pilote;
use App\Entity\Rallye;
use App\Entity\Speciale;
use App\Entity\Voiture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Persistence\ManagerRegistry;
use MongoDB\Driver\Manager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use function Sodium\add;

class CrudController extends AbstractController
{
    /**
     * @Route("/voiture/update/{id}",name="updatev")
     * @Route("/voiture/insert",name="newv")
     */
    public function updatev(Voiture $vor=null, Request $request){
        if(!$vor){
            $vor=new Voiture();
        }
        $manager=$this->getDoctrine()->getManager();
        $form=$this->createFormBuilder($vor)
            ->add('ModeleVoiture')
            ->add('IdEcurie',EntityType::class,['class'=>Ecurie::class,'choice_label'=>'NomEcurie'])
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($vor);
            $manager->flush();
            return $this->redirectToRoute('voiture');
        }

        return $this->render('crud/updatev.html.twig',['form'=>$form->createView(),'bool'=>$vor->getId()==null]);
    }
    /**
     * @Route("/speciale/update/{id}",name="updates")
     * @Route("/speciale/insert",name="news")
     */
    public function updates(Speciale $spe=null, Request $request){
        if(!$spe){
            $spe=new Speciale();
        }
        $manager=$this->getDoctrine()->getManager();
        $form=$this->createFormBuilder($spe)
            ->add('LieuSpeciale')
            ->add('HeureDebut')
            ->add('HeureFin')
            ->add('DateSpeciale')
            ->add('DistanceSpeciale')
            ->add('IdRallye',EntityType::class,['class'=>Rallye::class,'choice_label'=>'NomRallye'])
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($spe);
            $manager->flush();
            return $this->redirectToRoute('speciale');
        }

        return $this->render('crud/updates.html.twig',['form'=>$form->createView(),'bool'=>$spe->getId()==null]);
    }

    /**
     * @Route("/voiture/delete/{id}",name="deletev")
     */

    public function deletev($id){
        $repo=$this->getDoctrine()->getRepository(Voiture::class);
        $voiture=$repo->find($id);
        $manager=$this->getDoctrine()->getManager();
        $manager->remove($voiture);
        $manager->flush();
        return $this->redirectToRoute('voiture');

    }
    /**
     * @Route("/speciale/delete/{id}",name="deletes")
     */

    public function deletes($id){
        $repo=$this->getDoctrine()->getRepository(Speciale::class);
        $speciale=$repo->find($id);
        $manager=$this->getDoctrine()->getManager();
        $manager->remove($speciale);
        $manager->flush();
        return $this->redirectToRoute('speciale');

    }
    /**
     * @Route("/voiture/{id}",name="showv")
     */
    public function showv($id){

        $repo=$this->getDoctrine()->getRepository(Voiture::class);
        $voiture=$repo->find($id);

        return $this->render('crud/voiture1.html.twig',["voiture"=>$voiture]);
    }

    /**
     * @Route("/speciale/{id}",name="showp")
     */
    public function showp($id){

        $repo=$this->getDoctrine()->getRepository(Speciale::class);
        $speciale=$repo->find($id);

        return $this->render('crud/speciale1.html.twig',["speciale"=>$speciale]);
    }

    /**
     * @Route("/voiture",name="voiture")
     */
    public function voiture(){
        $repo=$this->getDoctrine()->getRepository(Voiture::class);
        $voitures=$repo->findAll();
        return $this->render('crud/voiture.html.twig',["voitures"=>$voitures]);
    }

    /**
     * @Route("/speciale",name="speciale")
     */
    public function speciale(){
        $repo=$this->getDoctrine()->getRepository(Speciale::class);
        $speciales=$repo->findAll();
        return $this->render('crud/speciale.html.twig',["speciales"=>$speciales]);

    }



    /**
     * @Route("/rallye/delete/{id}",name="deleter")
     */

    public function deleter($id){
        $repo=$this->getDoctrine()->getRepository(Rallye::class);
        $rallye=$repo->find($id);
        $manager=$this->getDoctrine()->getManager();
        $manager->remove($rallye);
        $manager->flush();
        return $this->redirectToRoute('rallye');
    }

    /**
     * @Route("/pilote/delete/{id}",name="deletep")
     */

    public function deletep($id){
        $repo=$this->getDoctrine()->getRepository(Pilote::class);
        $pilote=$repo->find($id);
        $manager=$this->getDoctrine()->getManager();
        $manager->remove($pilote);
        $manager->flush();
        return $this->redirectToRoute('pilote');

    }

    /**
     * @Route("/ecurie/delete/{id}",name="deletee")
     */

    public function deletee($id){
        $repo=$this->getDoctrine()->getRepository(Ecurie::class);
        $ecurie=$repo->find($id);
        $manager=$this->getDoctrine()->getManager();
        $manager->remove($ecurie);
        $manager->flush();
        return $this->redirectToRoute('ecurie');

    }


    /**
     * @Route("/rallye/update/{id}",name="updater")
     * @Route("/rallye/insert",name="newr")
     */
    public function updater(Rallye $ral=null, Request $request){
        if(!$ral){
            $ral=new Rallye();
        }
      $manager=$this->getDoctrine()->getManager();
        $form=$this->createFormBuilder($ral)
            ->add('Nom_Rallye')
            ->add('Lieu_Rallye')
            ->add('Piste_Rallye')
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($ral);
            $manager->flush();
            return $this->redirectToRoute('rallye');
        }

        return $this->render('crud/updater.html.twig',['form'=>$form->createView(),'bool'=>$ral->getId()==null]);
    }

    /**
     * @Route("/pilote/update/{id}",name="updatep")
     * @Route("/pilote/insert",name="newp")
     */
    public function updatep(Pilote $pil=null,Request $request){
        if(!$pil) {
            $pil = new Pilote();
        }
        $manager=$this->getDoctrine()->getManager();
        $pil = new Pilote();
        $form=$this->createFormBuilder($pil)
            ->add('Nom_Pilote')
            ->add('Prenom_Pilote')
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($pil);
            $manager->flush();
            return $this->redirectToRoute('pilote');
        }
        return $this->render('crud/updatep.html.twig',['form'=>$form->createView(),'bool'=>$pil->getId()==null]);
    }

    /**
     * @Route("/ecurie/update/{id}",name="updatee")
     * @Route("/ecurie/insert",name="newe")
     */
    public function updatee(Ecurie $ecu=null,Request $request){
        if(!$ecu){
            $ecu=new Ecurie();
        }
        $manager=$this->getDoctrine()->getManager();

        $form=$this->createFormBuilder($ecu)
            ->add('Nom_Ecurie')
            ->add('Nb_Titre')
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($ecu);
            $manager->flush();
            return $this->redirectToRoute('ecurie');
        }
        return $this->render('crud/updatee.html.twig',['form'=>$form->createView(),'bool'=>$ecu->getId()==null]);
    }
    /**
     * @Route("/crud", name="crud")
     */
    public function index()
    {
        return $this->render('crud/index.html.twig', [
            'controller_name' => 'CrudController',
        ]);
    }

    /**
     * @Route("/",name="home")
     */
    public function home(){
         return $this->render('crud/home.html.twig');
    }

    /**
     * @Route("/pilote",name="pilote")
     */


    public function pilote(){
        $repo=$this->getDoctrine()->getRepository(Pilote::class);
        $pilotes=$repo->findAll();
        return $this->render('crud/pilote.html.twig',['pilotes'=>$pilotes]);
    }

    /**
     * @Route("/pilote/{id}",name="pilote1")
     */
    public function pilote1($id){
        $repo=$this->getDoctrine()->getRepository(Pilote::class);
        $pilote=$repo->find($id);
        return $this->render('crud/pilote1.html.twig',['pilote'=>$pilote]);
    }


    /**
     * @Route("/ecurie",name="ecurie")
     */
    public function ecurie(){
        $repos=$this->getDoctrine()->getRepository(Ecurie::class);
        $ecuries=$repos->findAll();
        return $this->render('crud/ecurie.html.twig',['ecuries'=>$ecuries]);
    }

    /**
     * @Route("/ecurie/{id}",name="ecurie1")
     */
    public function ecurie1($id){
        $repos=$this->getDoctrine()->getRepository(Ecurie::class);
        $ecurie=$repos->find($id);
        return $this->render('crud/ecurie1.html.twig',['ecurie'=>$ecurie]);
    }


    /**
     * @Route("/rallye",name="rallye")
     */
    public function rallye(){
        $repos=$this->getDoctrine()->getRepository(Rallye::class);
        $rallyes=$repos->findAll();
        return $this->render('crud/rallye.html.twig',['rallyes'=>$rallyes]);
    }

    /**
     * @Route("/rallye/{id}",name="rallye2")
     */
    public function rallye2($id){
        $repos=$this->getDoctrine()->getRepository(Rallye::class);
        $rallye=$repos->find($id);
        return $this->render('crud/rallye2.html.twig',['rallye'=>$rallye]);
    }

}

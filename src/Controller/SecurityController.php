<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Form\FormRegistrationType;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/security", name="security")
     */
    public function index()
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @Route("/inscription",name="inscription")
     */

    public function inscription(Request $request){
        $manager=$this->getDoctrine()->getManager();
        $Admin=new Admin();
        $form=$this->createForm(FormRegistrationType::class,$Admin);
          $form->handleRequest($request);
          if($form->isSubmitted()&&$form->isValid()){
              $manager->persist($Admin);
              $manager->flush();
            return  $this->redirectToRoute('connexion');
          }
        return $this->render('security/inscription.html.twig',['form'=>$form->createView()]);
    }
    /**
     * @Route("/login",name="connexion")
     */
    public function connexion(){
        return $this->render('security/login.html.twig');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200318203437 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE speciale (id INT AUTO_INCREMENT NOT NULL, id_rallye_id INT NOT NULL, date_speciale DATE NOT NULL, heure_debut TIME NOT NULL, heure_fin TIME NOT NULL, lieu_speciale VARCHAR(255) NOT NULL, distance_speciale INT NOT NULL, INDEX IDX_D695A2A7F63293E2 (id_rallye_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE speciale ADD CONSTRAINT FK_D695A2A7F63293E2 FOREIGN KEY (id_rallye_id) REFERENCES rallye (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE speciale');
    }
}

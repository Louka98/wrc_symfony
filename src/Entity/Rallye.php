<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RallyeRepository")
 */
class Rallye
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_Rallye;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Piste_Rallye;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Lieu_Rallye;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomRallye(): ?string
    {
        return $this->Nom_Rallye;
    }

    public function setNomRallye(string $Nom_Rallye): self
    {
        $this->Nom_Rallye = $Nom_Rallye;

        return $this;
    }

    public function getPisteRallye(): ?string
    {
        return $this->Piste_Rallye;
    }

    public function setPisteRallye(string $Piste_Rallye): self
    {
        $this->Piste_Rallye = $Piste_Rallye;

        return $this;
    }

    public function getLieuRallye(): ?string
    {
        return $this->Lieu_Rallye;
    }

    public function setLieuRallye(string $Lieu_Rallye): self
    {
        $this->Lieu_Rallye = $Lieu_Rallye;

        return $this;
    }
}

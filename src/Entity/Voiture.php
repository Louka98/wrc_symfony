<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoitureRepository")
 */
class Voiture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ModeleVoiture;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ecurie")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idEcurie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModeleVoiture(): ?string
    {
        return $this->ModeleVoiture;
    }

    public function setModeleVoiture(string $ModeleVoiture): self
    {
        $this->ModeleVoiture = $ModeleVoiture;

        return $this;
    }

    public function getIdEcurie(): ?Ecurie
    {
        return $this->idEcurie;
    }

    public function setIdEcurie(?Ecurie $idEcurie): self
    {
        $this->idEcurie = $idEcurie;

        return $this;
    }
}

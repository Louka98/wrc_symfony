<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SpecialeRepository")
 */
class Speciale
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date_speciale;

    /**
     * @ORM\Column(type="time")
     */
    private $HeureDebut;

    /**
     * @ORM\Column(type="time")
     */
    private $HeureFin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $LieuSpeciale;

    /**
     * @ORM\Column(type="integer")
     */
    private $distanceSpeciale;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rallye")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idRallye;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateSpeciale(): ?\DateTimeInterface
    {
        return $this->date_speciale;
    }

    public function setDateSpeciale(\DateTimeInterface $date_speciale): self
    {
        $this->date_speciale = $date_speciale;

        return $this;
    }

    public function getHeureDebut(): ?\DateTimeInterface
    {
        return $this->HeureDebut;
    }

    public function setHeureDebut(\DateTimeInterface $HeureDebut): self
    {
        $this->HeureDebut = $HeureDebut;

        return $this;
    }

    public function getHeureFin(): ?\DateTimeInterface
    {
        return $this->HeureFin;
    }

    public function setHeureFin(\DateTimeInterface $HeureFin): self
    {
        $this->HeureFin = $HeureFin;

        return $this;
    }

    public function getLieuSpeciale(): ?string
    {
        return $this->LieuSpeciale;
    }

    public function setLieuSpeciale(string $LieuSpeciale): self
    {
        $this->LieuSpeciale = $LieuSpeciale;

        return $this;
    }

    public function getDistanceSpeciale(): ?int
    {
        return $this->distanceSpeciale;
    }

    public function setDistanceSpeciale(int $distanceSpeciale): self
    {
        $this->distanceSpeciale = $distanceSpeciale;

        return $this;
    }

    public function getIdRallye(): ?Rallye
    {
        return $this->idRallye;
    }

    public function setIdRallye(?Rallye $idRallye): self
    {
        $this->idRallye = $idRallye;

        return $this;
    }
}

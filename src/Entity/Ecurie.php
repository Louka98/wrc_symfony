<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EcurieRepository")
 */
class Ecurie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_Ecurie;

    /**
     * @ORM\Column(type="integer")
     */
    private $Nb_Titre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEcurie(): ?string
    {
        return $this->Nom_Ecurie;
    }

    public function setNomEcurie(string $Nom_Ecurie): self
    {
        $this->Nom_Ecurie = $Nom_Ecurie;

        return $this;
    }

    public function getNbTitre(): ?int
    {
        return $this->Nb_Titre;
    }

    public function setNbTitre(int $Nb_Titre): self
    {
        $this->Nb_Titre = $Nb_Titre;

        return $this;
    }
}

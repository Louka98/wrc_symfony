<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PiloteRepository")
 */
class Pilote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_Pilote;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenom_Pilote;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPilote(): ?string
    {
        return $this->Nom_Pilote;
    }

    public function setNomPilote(string $Nom_Pilote): self
    {
        $this->Nom_Pilote = $Nom_Pilote;

        return $this;
    }

    public function getPrenomPilote(): ?string
    {
        return $this->Prenom_Pilote;
    }

    public function setPrenomPilote(string $Prenom_Pilote): self
    {
        $this->Prenom_Pilote = $Prenom_Pilote;

        return $this;
    }
}
